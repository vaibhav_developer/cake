<?php 
$config['ADMIN_PAGINATION'] = 2;
$config['CATEGORY.MAIN_IMAGE'] = WWW_ROOT.'img/category/';
$config['CATEGORY.THUMB'] = WWW_ROOT.'img/category/thumb/';
$config['CATEGORY.LARGE'] = WWW_ROOT.'img/category/large/';
$config['CATEGORY.THUMB_WIDTH'] = 50;
$config['CATEGORY.THUMB_HEIGHT'] = 50;
$config['CATEGORY.LARGE_WIDTH'] = 100;
$config['CATEGORY.LARGE_HEIGHT'] = 75;

///////////////////////////////////////////////////////////////////////////////////FOR PRODUCT
$config['PRODUCT.MAIN_IMAGE'] = WWW_ROOT.'img/PRODUCT/';
$config['PRODUCT.THUMB'] = WWW_ROOT.'img/PRODUCT/thumb/';
$config['PRODUCT.LARGE'] = WWW_ROOT.'img/PRODUCT/large/';
$config['PRODUCT.THUMB_WIDTH'] = 50;
$config['PRODUCT.THUMB_HEIGHT'] = 50;
$config['PRODUCT.LARGE_WIDTH'] = 100;
$config['PRODUCT.LARGE_HEIGHT'] = 75;

///////////////////////////////////////////////////////////////////////////////////FOR PRODUCT
$config['PRODUCT.ADDITIONAL_IMAGES'] = WWW_ROOT.'img/PRODUCT/Additionalimages/';
$config['PRODUCT.ADDITIONAL_THUMB'] = WWW_ROOT.'img/PRODUCT/Additionalimages/thumb/';
$config['PRODUCT.ADDITIONAL_LARGE'] = WWW_ROOT.'img/PRODUCT/Additionalimages/large/';
$config['PRODUCT.ADDITIONAL_THUMB_WIDTH'] = 50;
$config['PRODUCT.ADDITIONAL_THUMB_HEIGHT'] = 50;
$config['PRODUCT.ADDITIONAL_LARGE_WIDTH'] = 100;
$config['PRODUCT.ADDITIONAL_LARGE_HEIGHT'] = 75;


