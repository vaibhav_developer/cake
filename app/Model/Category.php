<?php 
App::uses('Model', 'Model');
class Category extends AppModel
{
	/* var $validate = array(
		'category_name'=>array(
			'rule'=>array('notEmpty',),
			'message'=>'this field is required'
		)
	); */
	var $validate = array(
	'category_name'=>array(
		'need'=>array(
			'rule'=>array('notEmpty'),
			'message'=>'This field is required.'
		),
		'lenth'=>array(
			'rule'=>array('minLength',3),
			'message'=>'Category name minimum 3 characters.'
		),
		'uni'=>array(
			'rule'=>array('isUnique'),
			'message'=>'category name already exist.'
			)
	),
	
		'category_image'=>array(
		'extension'=>array(
			'rule'=>array('extension',array('png','jpg','jpeg')),
			'message'=>'please upload valid file.'	
		),
		'image_size'=>array(
			'rule' => array('fileSize', '<=', '1MB'),
			'message' => 'Image must be less than 1MB'
		)
		
	)
	
	
	);
}


?>