<?php 
App::uses('Model', 'Model');
class Product extends AppModel
{
	/* var $validate = array(
		'category_name'=>array(
			'rule'=>array('notEmpty',),
			'message'=>'this field is required'
		)
	); */
	
	var $validate=array(
		'product_name'=>array(
			'Required'=>array(
				'rule'=>array('notEmpty'),
				'message'=>'This fild is required.'
			),
			'lenth'=>array(
				'rule'=>array('minLength',3),
				'message'=>'The Minimus Length 3 Character.'
			),
			'required'=>array(
			'rule'=>array('isUnique'),
			'message'=>'category name already exist.'
			)
		),
		'category_id'=>array(
			'required'=>array(
				'rule'=>array('notEmpty'),
				'message'=>'Please Select Category.'
			
			)
		),
		'product_price'=>array(
				'need'=>array(
					'rule'=>array('notEmpty'),
					'message'=>'Please Fill Price Fild.'
					),
				'type'=>array(
					'rule' =>array('numeric'),
					'message' => 'Please Enter Only number .'
				
				),
				'length'=>array(
					'rule' =>array('minLength',2),
					'message' => 'Please fill at Lest 2 number.'
				
				)
		),
		'product_code'=>array(
				'need'=>array(
					'rule'=>array('notEmpty'),
					'message'=>'Please Fill product_code Fild.'
					),
				'type'=>array(
					'rule' =>array('numeric'),
					'message' => 'Please Enter Only number .'
				
				),
				'length'=>array(
					'rule' =>array('minLength',2),
					'message' => 'Please fill at Lest 2 number.'
				
				)
			)
		
		
	);
	
	
	
	
	
}


?>