<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( "#dialog" ).dialog({
      autoOpen: true,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
 
    $( "#opener" ).click(function() {
      $( "#dialog" ).dialog( "open" );
    });
  });
  </script>



 <div id="sidebar"><div class="sidebar_top"></div><div class="sidebar_bottom"></div>
    	
        <div class="sidebar_section">
        
            <h2>Members</h2>
            
            
			<?php 
					echo $this->Form->create('User',array('inputDefaults'=>array('div'=>false),'novalidate'=>true));
					echo $this->Form->input('email',array('class'=>'input_field','placehoder'=>'Email Address'));
					echo $this->Form->input('password',array('class'=>'input_field','placehoder'=>'Password'));
                    echo $this->Html->link('<b>Register</b>',array('admin'=>false,'controller'=>'Users','action'=>''),array('escape'=>false,'id'=>'opener'));
					echo $this->Form->submit('Login',array('id'=>'submit_btn','div'=>false));
					echo $this->Form->end();
			
			?>
            
			<div class="cleaner"></div>
            
		</div>

  
<div id="dialog" title="Basic dialog">
  <p>This is an animated dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
</div>		
        
        <div class="sidebar_section">
        
        	<h2>Categories</h2>
                    
            <ul class="categories_list">
                <li><a href="#">Lorem ipsum dolor</a></li>
                <li><a href="#">Phasellus eget lorem</a></li>
                <li><a href="#">Sed sit amet sem</a></li>
                <li><a href="#">Cras eget est vel</a></li>
                <li><a href="#">Quisque in ligula</a></li>
                <li><a href="#">Donec a massa dui</a></li>
                <li><a href="#">Aenean facilisis</a></li>
                <li><a href="#">Etiam vitae consequat</a></li>
                <li><a href="#">Aliquam sollicitudin</a></li>
                <li><a href="#">Nunc fermentum</a></li>
            </ul>
        </div>
        
        <div class="sidebar_section">
        
            <h2>Discounts</h2>
            
            <div class="image_wrapper"><a href="http://www.templatemo.com/page/7" target="_parent">
			<?php echo $this->Html->image('image_01.jpg');?>
			
			</a></div>            
          <div class="discount"><span>25% off</span> | <a href="#">Read more</a></div>
        
        </div>  
        
    </div> <!-- end of sidebar -->