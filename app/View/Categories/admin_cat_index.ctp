
<!--  start page-heading -->
	<div id="page-heading">
		<h1>Categories</h1>
	</div>
	<!-- end page-heading -->
<!--<?php pr($categories);?> -->
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized">
		<?php echo $this->Html->image('admin/side_shadowleft.jpg',array('width'=>20, 'height'=>'300')); ?>
		<img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized">
		<?php echo $this->Html->image('admin/side_shadowright.jpg',array('width'=>20, 'height'=>'300')); ?>
		</th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
			
				<!--  start product-table ..................................................................................... -->
				<form id="mainform" action="">
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
				<tr>
					<th class="table-header-check"><a id="toggle-all" ></a> </th>
					<th class="table-header-repeat line-left minwidth-1"><a href="">Image</a>	</th>
					<th class="table-header-repeat line-left minwidth-1"><a href="">Category Name</a></th>
					<th class="table-header-repeat line-left"><a href="">Date</a></th>
					<th class="table-header-repeat line-left"><a href="">Status</a></th>
					<th class="table-header-options line-left"><a href="">Options</a></th>
				</tr>
				<?php if(!empty($categories)){
					$i = 0;
					foreach($categories as $value){
					//$class = ($i%2==0)?'':'class="alternate-row"';
					
					?>
				<tr <?php //echo $class;?>>
					<td><input  type="checkbox"/></td>
					<td><?php echo $this->Html->image('category/thumb/'.$value['Category']['category_image']);?></td>
					<td><?php echo $value['Category']['category_name']?></td>
					<td><?php echo $value['Category']['created']?></td>
					<td><?php echo ($value['Category']['status']==1)?'Active':'inActive';?></td>
					<td class="options-width">
					<?php echo $this->Html->link('', array('admin' => true, 'controller' => 'Categories', 'action' => 'edit_category', $value['Category']['id']), array('escape' => false, 'class' => 'icon-1 info-tooltip', 'title' => 'Edit')); ?>
					<a href="" title="Edit" class="icon-2 info-tooltip"></a>
					<a href="" title="Edit" class="icon-3 info-tooltip"></a>
					<a href="" title="Edit" class="icon-4 info-tooltip"></a>
					<a href="" title="Edit" class="icon-5 info-tooltip"></a>
					</td>
				</tr>
				<?php $i++;}
					} else{?>
					<tr><td colspan="6">No Record Found.</td> </tr>
			<?php }		
				?>

					
				</table>
				<!--  end product-table................................... --> 
				</form>
			</div>
			<!--  end content-table  -->
		
			<!--  start actions-box ............................................... -->
			<div id="actions-box">
				<a href="" class="action-slider"></a>
				<div id="actions-box-slider">
					<a href="" class="action-edit">Edit</a>
					<a href="" class="action-delete">Delete</a>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end actions-box........... -->
			
			<?php echo $this->element('admin_pagination'); ?>
			
			<div class="clear"></div>
		 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>
	<div class="clear">&nbsp;</div>