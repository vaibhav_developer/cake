<div id="page-heading"><h1>Add product</h1></div>


<table cellspacing="0" cellpadding="0" border="0" width="100%" id="content-table">
    <tbody><tr>
            <th class="sized" rowspan="3"><img width="20" height="300" alt="" src="images/shared/side_shadowleft.jpg"></th>
            <th class="topleft"></th>
            <td id="tbl-border-top">&nbsp;</td>
            <th class="topright"></th>
            <th class="sized" rowspan="3"><img width="20" height="300" alt="" src="images/shared/side_shadowright.jpg"></th>
        </tr>
        <tr>
            <td id="tbl-border-left"></td>
            <td>
                <!--  start content-table-inner -->
                <div id="content-table-inner">

                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tbody><tr valign="top">
                                <td>


                                    <!--  start step-holder -->
                                    <div id="step-holder">
                                        <div class="step-no">1</div>
                                        <div class="step-dark-left"><a href="">Add product details</a></div>
                                        <div class="step-dark-right">&nbsp;</div>
                                        <div class="step-no-off">2</div>
                                        <div class="step-light-left">Select related products</div>
                                        <div class="step-light-right">&nbsp;</div>
                                        <div class="step-no-off">3</div>
                                        <div class="step-light-left">Preview</div>
                                        <div class="step-light-round">&nbsp;</div>
                                        <div class="clear"></div>
                                    </div>
                                    <!--  end step-holder -->

                                    <!-- start id-form -->
                                    <?php echo $this->Form->create('Product', array('type' => 'file', 'novalidate' => true, 'inputDefaults' => array('div' => false, 'label' => false))); ?>
                                    <?php echo $this->Form->hidden('id'); ?>

                                    <table cellspacing="0" cellpadding="0" border="0" id="id-form">
                                        <tbody>
                                            <tr>
                                                <th valign="top">Product name:</th>
                                                <td>
                                                    <?php echo $this->Form->input('product_name', array('class' => 'inp-form')); ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th valign="top">Product Code:</th>
                                                <td>
                                                    <?php echo $this->Form->input('product_code', array('type' => 'text', 'class' => 'inp-form')); ?>

                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th valign="top">Category name:</th>

                                                <td>           
                                                    <?php
                                                    //pr($categories);
                                                    /* $cat = array();
                                                      foreach($categories as $values){
                                                      $cat[$values['Category']['id']]= $values['Category']['category_name'];
                                                      } */

                                                    echo $this->Form->input('category_id', array('options' => $categories, 'empty' => 'Select Category'));
                                                    ?>


                                                </td>


                                                <td></td>
                                            </tr>

                                            <tr>
                                                <th valign="top">Price:</th>
                                                <td>
<?php echo $this->Form->input('product_price', array('type' => 'text', 'class' => 'inp-form')); ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th valign="top">Description:</th>
                                                <td>
                                                    <?php echo $this->Form->textarea('product_description', array('rows' => '', 'cols' => '', 'class' => 'form-textarea')); ?>

                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th valign="top">Product Image:</th>
                                                <td>
                                                    <?php echo $this->Form->input('product_image', array('type' => 'file', 'class' => '')); ?>
                                                </td>
                                                <td><?php
                                                    echo $this->Html->image('PRODUCT/thumb/' . $this->request->data['Product']['product_image']);
                                                    echo $this->Form->hidden('product_prev_image', array('value' => $this->request->data['Product']['product_image']));
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th valign="top">Product Images:</th>
                                                <td>
<?php echo $this->Form->input('additional_images][', array('type' => 'file', 'class' => '', 'multiple')); ?>


                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th valign="top">&nbsp;</th>
                                                <td colspan="2">
<?php if (isset($this->request->data['ProductImage'])) { ?>
                                                        <ul class="additional_images">
                                                        <?php foreach ($this->request->data['ProductImage'] as $value) { ?>

                                                                <li>

        <?php
        echo $this->Html->link($this->Html->image('admin/icon_minus.gif'), 'javascript:void(0);', array('escape' => false, 'class' => 'cross', 'id' => $value['id'], 'data-id' => $value['images']));
        echo $this->Html->image('PRODUCT/Additionalimages/thumb/' . $value['images']);
        ?></li>


    <?php } ?>
                                                        </ul> <?php
                                                        }
                                                        ?>
                                                </td>

                                            </tr>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <td valign="top">
<?php echo $this->Form->submit('Submit', array('class' => 'form-submit')); ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody></table>
                                    <!-- end id-form  -->
<?php echo $this->Form->end(); ?>

                                </td>
                                <td>

                                    <!--  start related-activities -->
                                    <div id="related-activities">

                                        <!--  start related-act-top -->
                                        <div id="related-act-top">
                                            <img width="271" height="43" alt="" src="images/forms/header_related_act.gif">
                                        </div>
                                        <!-- end related-act-top -->

                                        <!--  start related-act-bottom -->
                                        <div id="related-act-bottom">

                                            <!--  start related-act-inner -->
                                            <div id="related-act-inner">

                                                <div class="left"><a href=""><img width="21" height="21" alt="" src="images/forms/icon_plus.gif"></a></div>
                                                <div class="right">
                                                    <h5>Add another product</h5>
                                                    Lorem ipsum dolor sit amet consectetur
                                                    adipisicing elitsed do eiusmod tempor.
                                                    <ul class="greyarrow">
                                                        <li><a href="">Click here to visit</a></li> 
                                                        <li><a href="">Click here to visit</a> </li>
                                                    </ul>
                                                </div>

                                                <div class="clear"></div>
                                                <div class="lines-dotted-short"></div>

                                                <div class="left"><a href=""><img width="21" height="21" alt="" src="images/forms/icon_minus.gif"></a></div>
                                                <div class="right">
                                                    <h5>Delete products</h5>
                                                    Lorem ipsum dolor sit amet consectetur
                                                    adipisicing elitsed do eiusmod tempor.
                                                    <ul class="greyarrow">
                                                        <li><a href="">Click here to visit</a></li> 
                                                        <li><a href="">Click here to visit</a> </li>
                                                    </ul>
                                                </div>

                                                <div class="clear"></div>
                                                <div class="lines-dotted-short"></div>

                                                <div class="left"><a href=""><img width="21" height="21" alt="" src="images/forms/icon_edit.gif"></a></div>
                                                <div class="right">
                                                    <h5>Edit categories</h5>
                                                    Lorem ipsum dolor sit amet consectetur
                                                    adipisicing elitsed do eiusmod tempor.
                                                    <ul class="greyarrow">
                                                        <li><a href="">Click here to visit</a></li> 
                                                        <li><a href="">Click here to visit</a> </li>
                                                    </ul>
                                                </div>
                                                <div class="clear"></div>

                                            </div>
                                            <!-- end related-act-inner -->
                                            <div class="clear"></div>

                                        </div>
                                        <!-- end related-act-bottom -->

                                    </div>
                                    <!-- end related-activities -->

                                </td>
                            </tr>
                            <tr>
                                <td><img width="695" height="1" alt="blank" src="images/shared/blank.gif"></td>
                                <td></td>
                            </tr>
                        </tbody></table>

                    <div class="clear"></div>


                </div>
                <!--  end content-table-inner  -->
            </td>
            <td id="tbl-border-right"></td>
        </tr>
        <tr>
            <th class="sized bottomleft"></th>
            <td id="tbl-border-bottom">&nbsp;</td>
            <th class="sized bottomright"></th>
        </tr>
    </tbody></table>



<div class="clear">&nbsp;</div>
<style>
    ul.additional_images li {
        border: 1px solid #dedede;
        float: left;
        list-style: outside none none;
        margin-right: 7px;
    }
    .cross{
        margin-left: 30px;
        position: absolute;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('.cross').live('click', function () {
            var $_current = $(this);
            if (confirm('Are you sure delete this image?')) {
                var id = $(this).attr('id');
                var image_name = $(this).attr('data-id');
                $.ajax({
                    url: "<?php echo Router::url(array('admin' => true, 'controller' => 'products', 'action' => 'delete_additional_images'), true); ?> ",
                    type: 'POST',
                    data: {image_id: id, image_name: image_name},
                    success: function (data) {
                        if (data == 'success') {
                            $_current.parent().remove();

                        }
                    }
                });
            }
        });
    })
</script>