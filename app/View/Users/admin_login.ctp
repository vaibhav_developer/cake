
<!--  start loginbox ................................................................................. -->
	<div id="loginbox">
	
	<!--  start login-inner -->
	<div id="login-inner">
	<?php echo  $this->Form->create('User',array('novalidate'=>true,'inputDefaults'=>array('div'=>false,'label'=>false)));?>
		<table border="0" cellpadding="0" cellspacing="0">
		
		<tr>
			<th>Email</th>
			<td><?php echo  $this->Form->input('email',array('class'=>'login-inp'));?></td>
		</tr>
		<tr>
			<th>Password</th>
			<td><?php echo  $this->Form->input('password',array('class'=>'login-inp'));?></td>
		</tr>
		<tr>
			<th></th>
			<td valign="top"><input type="checkbox" class="checkbox-size" id="login-check" /><label for="login-check">Remember me</label></td>
		</tr>
		<tr>
			<th></th>
			<td><?php echo  $this->Form->submit('Submit',array('class'=>'submit-login'));?></td>
		</tr>
		</table>
		<?php echo  $this->Form->end();
	?>
	</div>
 	<!--  end login-inner -->
	<div class="clear"></div>
	<?php echo $this->Html->link('Forgot Password','javascript:void(0);',array('class'=>'forgot-pwd'));?>
 </div>
 <!--  end loginbox -->
 
 <script type="text/javascript">
	$(document).ready(function(){
		$('.close-red').click(function(){
			$('#message-red').hide('slow');
		});
	});
 </script>
