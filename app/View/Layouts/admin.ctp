<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version());
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		
		
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		//echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');?>

		<?php echo $this->fetch('meta');?>
		<?php echo $this->fetch('css');?>
		<?php echo $this->fetch('script');?>
		<?php echo $this->Html->css(array('admin/screen'));?>
		<?php echo $this->Html->script(array('jquery/jquery-1.4.1.min'));?>
		<?php echo $this->Html->script(array('jquery/ui.core'));?>
		<?php echo $this->Html->script(array('jquery/ui.checkbox'));?>
		<?php echo $this->Html->script(array('jquery/jquery.bind'));?>
		<script type="text/javascript">
			$(function(){
			/*$('input').checkBox();
			$('#toggle-all').click(function(){
			$('#toggle-all').toggleClass('toggle-checked');
			$('#mainform input[type=checkbox]').checkBox('toggle');
			return false;
			});*/
		});
		</script>  
		
		<?php echo $this->Html->script(array('jquery/jquery.selectbox-0.5'));?>
		<script type="text/javascript">
		$(document).ready(function() {
			$('.styledselect').selectbox({ inputClass: "selectbox_styled" });
		});
		</script>
		<?php echo $this->Html->script(array('jquery/jquery.selectbox-0.5_style_2'));?>
		<script type="text/javascript">
		$(document).ready(function() {
			$('.styledselect_form_1').selectbox({ inputClass: "styledselect_form_1" });
			$('.styledselect_form_2').selectbox({ inputClass: "styledselect_form_2" });
		});
		</script>
		<?php echo $this->Html->script(array('jquery/jquery.selectbox-0.5_style_2'));?>
		<script type="text/javascript">
		$(document).ready(function() {
			$('.styledselect_pages').selectbox({ inputClass: "styledselect_pages" });
		});
		</script>
		<?php echo $this->Html->script(array('jquery/jquery.filestyle'));?>
		<script type="text/javascript" charset="utf-8">
		$(function() {
			$("input.file_1").filestyle({ 
			image: "images/forms/upload_file.gif",
			imageheight : 29,
			imagewidth : 78,
			width : 300
			});
		});
		</script>
		<?php echo $this->Html->script(array('jquery/custom_jquery'));?>
		<?php echo $this->Html->script(array('jquery/jquery.tooltip'));?>
		<?php echo $this->Html->script(array('jquery/jquery.dimensions'));?>
		<script type="text/javascript">
		$(function() {
			$('a.info-tooltip ').tooltip({
				track: true,
				delay: 0,
				fixPNG: true, 
				showURL: false,
				showBody: " - ",
				top: -35,
				left: 5
			});
		});
		</script> 
		<?php echo $this->Html->css(array('admin/datePicker'));?>
		<?php echo $this->Html->script(array('jquery/date'));?>
		<?php echo $this->Html->script(array('jquery/jquery.datePicker'));?>
		<script type="text/javascript" charset="utf-8">
				$(function()
		{
		
		// initialise the "Select date" link
		$('#date-pick')
			.datePicker(
				// associate the link with a date picker
				{
					createButton:false,
					startDate:'01/01/2005',
					endDate:'31/12/2020'
				}
			).bind(
				// when the link is clicked display the date picker
				'click',
				function()
				{
					updateSelects($(this).dpGetSelected()[0]);
					$(this).dpDisplay();
					return false;
				}
			).bind(
				// when a date is selected update the SELECTs
				'dateSelected',
				function(e, selectedDate, $td, state)
				{
					updateSelects(selectedDate);
				}
			).bind(
				'dpClosed',
				function(e, selected)
				{
					updateSelects(selected[0]);
				}
			);
			
		var updateSelects = function (selectedDate)
		{
			var selectedDate = new Date(selectedDate);
			$('#d option[value=' + selectedDate.getDate() + ']').attr('selected', 'selected');
			$('#m option[value=' + (selectedDate.getMonth()+1) + ']').attr('selected', 'selected');
			$('#y option[value=' + (selectedDate.getFullYear()) + ']').attr('selected', 'selected');
		}
		// listen for when the selects are changed and update the picker
		$('#d, #m, #y')
			.bind(
				'change',
				function()
				{
					var d = new Date(
								$('#y').val(),
								$('#m').val()-1,
								$('#d').val()
							);
					$('#date-pick').dpSetSelected(d.asString());
				}
			);
		
		// default the position of the selects to today
		var today = new Date();
		updateSelects(today.getTime());
		
		// and update the datePicker to reflect it...
		$('#d').trigger('change');
		});
		</script>

		<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
		<?php echo $this->Html->script(array('jquery/jquery.pngFix.pack'));?>
		<script type="text/javascript">
		$(document).ready(function(){
		$(document).pngFix( );
		});
		</script>
	
</head>
<body>

		<?php echo $this->element('admin_header');?>
		<div id="content-outer">
			<div id="content">
			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
		<div id="footer">
			
			<p>
				//<?php echo $cakeVersion; ?>
			</p>
		</div>
	
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
