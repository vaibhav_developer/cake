<!--  start page-heading -->
<div id="page-heading">
    <h1>Product</h1>
</div>
<!-- end page-heading -->
 <?php// pr($page); ?> 
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
    <tr>
        <th rowspan="3" class="sized">
            <?php echo $this->Html->image('admin/side_shadowleft.jpg', array('width' => 20, 'height' => '300')); ?>
            <img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized">
            <?php echo $this->Html->image('admin/side_shadowright.jpg', array('width' => 20, 'height' => '300')); ?>
        </th>
    </tr>
    <tr>
        <td id="tbl-border-left"></td>
        <td>
            <!--  start content-table-inner ...................................................................... START -->
            <div id="content-table-inner">

                <!--  start table-content  -->
                <div id="table-content">

                    <!--  start product-table ..................................................................................... -->

                    <?php echo $this->Form->create('Product', array('url' => array('admin' => true, 'controller' => 'pages', 'action' => 'process'), 'id' => 'mainform')); ?>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
                        <tr>
                            <th class="table-header-check"><?php echo $this->Form->checkbox('all', array('hiddenField' => false, 'class' => 'select_all', 'id' => 'toggle-all')); ?></th>
                            <th class="table-header-repeat line-left minwidth-1"><a href="">Title</a>	</th>
                            <th class="table-header-repeat line-left minwidth-1"><a href="">Heading</a></th>
                            <th class="table-header-repeat line-left minwidth-1"><a href="">Meta Title</a></th>
                            <th class="table-header-repeat line-left minwidth-1"><a href="">Meta Key</a></th>
							<th class="table-header-repeat line-left minwidth-1"><a href="">Meta Descriiption</a></th>
                            <th class="table-header-repeat line-left minwidth-1"><a href="">Description</a></th>
                            <th class="table-header-repeat line-left"><a href="">Date</a></th>
                            <th class="table-header-repeat line-left"><a href="">Status</a></th>
                            <th class="table-header-options line-left"><a href="">Options</a></th>
                        </tr>
                        <?php
                        if (!empty($page)) {
                            //pr($product);
                            $i = 0;
                            foreach ($page as $value) {
                                //$class = ($i%2==0)?'':'class="alternate-row"';
                                ?>
                                <tr <?php
                                //pr($this->Session->read('Auth.User.f_name'));
                                //echo $class;
                                ?>>
                                    <td><?php echo $this->Form->checkbox('Page.id][', array('hiddenField' => false, 'class' => 'page_ids', 'value' => $value['Page']['id'])); ?></td>
                                    
                                    <td><?php echo $value['Page']['page_title'] ?></td>
                                    <td><?php echo $value['Page']['page_heading'] ?></td>
                                    <td><?php echo $value['Page']['meta_title'] ?></td>
									
									<td><?php echo $value['Page']['meta_key'] ?></td>
                                    <td><?php echo $value['Page']['meta_description'] ?></td>
									<td><?php echo $value['Page']['description'] ?></td>
                                    <td><?php echo $value['Page']['created'] ?></td>
                                    <td><?php echo ($value['Page']['status'] == 1) ? $this->Html->link('Active', array('admin' => true, 'controller' => 'Page', 'action' => 'status', $value['Page']['id'], $value['Page']['status']), array('escape' => false, 'class' => '', 'title' => '')) : $this->Html->link('inActive', array('admin' => true, 'controller' => 'Page', 'action' => 'status', $value['Page']['id'], $value['Page']['status']), array('escape' => false, 'class' => '', 'title' => '')); ?></td>
                                    <td class="options-width">
                                        <?php echo $this->Html->link('', array('admin' => true, 'controller' => 'pages', 'action' => 'edit', $value['Page']['id']), array('escape' => false, 'class' => 'icon-1 info-tooltip', 'title' => 'Edit')); ?>

                                        <a href="" title="Edit" class="icon-2 info-tooltip"></a>
                                        <a href="" title="Edit" class="icon-3 info-tooltip"></a>
                                        <a href="" title="Edit" class="icon-4 info-tooltip"></a>
                                        <a href="" title="Edit" class="icon-5 info-tooltip"></a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        } else {
                            ?>
                            <tr><td colspan="6">No Record Found.</td> </tr>
                        <?php }
                        ?>


                    </table>
                    <?php echo $this->Form->hidden('action', array('hiddenField' => false)); ?>
                    <!--  end product-table................................... --> 
<?php echo $this->Form->end(); ?>
                </div>
                <!--  end content-table  -->

                <!--  start actions-box ............................................... -->
                <div id="actions-box">
                    <a href="" class="action-slider"></a>
                    <div id="actions-box-slider">
                        <?php
                        echo $this->Html->link('Active', 'javascript:void(0);', array('class' => 'action-edit action'));
                        echo $this->Html->link('Deactivate', 'javascript:void(0);', array('class' => 'action-edit action'));
                        echo $this->Html->link('Delete', 'javascript:void(0);', array('class' => 'action-delete action'));
                        ?>

                    </div>
                    <div class="clear"></div>
                </div>
                <!-- end actions-box........... -->

<?php echo $this->element('admin_pagination'); ?>

                <div class="clear"></div>

            </div>
            <!--  end content-table-inner ............................................END  -->
        </td>
        <td id="tbl-border-right"></td>
    </tr>
    <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
    </tr>
</table>
<div class="clear">&nbsp;</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.select_all').live('click', function () {
            if ($(this).is(':checked')) {
                //alert(1);
                $('.product_ids').attr('checked', true)
            } else {
                //alert(0);
                $('.product_ids').attr('checked', false);
            }
        });

        $('.product_ids').live('click', function () {
            if ($('.product_ids').length == $('.product_ids:checked').length) {
                $('.select_all').attr('checked', true);
            } else {
                $('.select_all').attr('checked', false)
            }

        });
        $('.action').click(function () {
            var action = ($(this).text()).toLowerCase();
            var checked_checkbox_len = $('.product_ids:checked').length;
            if (checked_checkbox_len == 0) {
                alert('Please select atleast one checkbox');
                return false;
            }
            $('#ProductAction').val(action);
            $('#mainform').submit();
        })
    });
</script> 