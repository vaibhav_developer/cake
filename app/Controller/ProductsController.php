<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 * 	
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		product.Controller

 */
class ProductsController extends AppController {

    var $components = array('Upload');

    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow(array('admin_add'));
    }

    public function admin_index() {
        $this->Product->bindModel(array('belongsTo' => array('Category'), 'hasMany' => array('ProductImage')));
        //$this->Product->bindModel(array('belongsTo'=>array('Category')));
        //$product = $this->Product->find('all');
        $this->Paginator->settings = array(
            'limit' => Configure::read('ADMIN_PAGINATION'),
            'order' => array('Product.product_name' => 'asc')
        );
        $product = $this->Paginator->paginate('Product');
        //pr($product);die;
        $this->set('product', $product);
    }

    public function admin_edit_product($id = null) {
        $this->loadModel('Category');
        $categories = $this->Category->find('list', array('conditions' => array('Category.status' => 1), 'fields' => array('id', 'category_name'), 'order' => array('Category.category_name')));
        $this->set('categories', $categories);

        //pr($this->request->data);die;
        if ($this->request->is('post') || $this->request->is('put')) {
            //pr($this->request->data);die;
            $this->Product->set($this->request->data);
            if ($this->Product->validates()) {
                $file = array();

                if (isset($this->request->data['Product']['product_image']['name']) && $this->request->data['Product']['product_image']['error'] == 0) {
                    $file = $this->request->data['Product']['product_image'];
                    $thumb = array('size' => array(Configure::read('PRODUCT.THUMB_WIDTH'), Configure::read('PRODUCT.THUMB_HEIGHT')), 'type' => 'resize');
                    $large = array('size' => array(Configure::read('PRODUCT.LARGE_WIDTH'), Configure::read('PRODUCT.LARGE_HEIGHT')), 'type' => 'resize');
                    $res = $this->Upload->upload($file, Configure::read('PRODUCT.THUMB') . DS, $thumb);
                    $resl = $this->Upload->upload($file, Configure::read('PRODUCT.LARGE') . DS, $large);
                    $res1 = $this->Upload->upload($file, Configure::read('PRODUCT.MAIN_IMAGE') . DS);
                    if ($this->Upload->result) {
                        //echo $this->Upload->result;die;
                        $this->request->data['Product']['product_image'] = $this->Upload->result;
                    }
                } else {
                    //echo "hi";die;
                    $this->request->data['Product']['product_image'] = $this->request->data['Product']['product_prev_image'];
                }
                $additional_data = array();
                if (isset($this->request->data['Product']['additional_images']) && !empty($this->request->data['Product']['additional_images']) && $this->request->data['Product']['additional_images'][0]['error'] == 0) {
                    $additional_images = array();
                    foreach ($this->request->data['Product']['additional_images'] as $key => $value) {
                        $additional_images = $this->request->data['Product']['additional_images'][$key];
                        $thumb = array('size' => array(Configure::read('PRODUCT.ADDITIONAL_THUMB_WIDTH'), Configure::read('PRODUCT.ADDITIONAL_THUMB_HEIGHT')), 'type' => 'resize');
                        $large = array('size' => array(Configure::read('PRODUCT.ADDITIONAL_LARGE_WIDTH'), Configure::read('PRODUCT.ADDITIONAL_LARGE_HEIGHT')), 'type' => 'resize');
                        $res = $this->Upload->upload($additional_images, Configure::read('PRODUCT.ADDITIONAL_THUMB') . DS, $thumb);
                        $resl = $this->Upload->upload($additional_images, Configure::read('PRODUCT.ADDITIONAL_LARGE') . DS, $large);
                        $res1 = $this->Upload->upload($additional_images, Configure::read('PRODUCT.ADDITIONAL_IMAGES') . DS);
                        if ($this->Upload->result) {

                            //echo $this->Upload->result;die;
                            $additional_data[] = $this->Upload->result;
                        }
                    }
                } else {
                    unset($this->request->data['Product']['additional_images']);
                }
                //pr($additional_data);die;

                $this->Product->id = $this->request->data['Product']['id'];
                if ($this->Product->save($this->request->data)) {
                    //pr($this->request->data);die;
                    $last_id = $this->Product->id;

                    if (!empty($additional_data)) {
                        $this->loadModel('ProductImage');
                        $additional = array();
                        foreach ($additional_data as $values) {
                            $additional['ProductImage']['product_id'] = $last_id;
                            $additional['ProductImage']['images'] = $values;
                            $this->ProductImage->create();
                            $this->ProductImage->save($additional);
                        }
                    }
                    $this->Session->setFlash('Product created successfully', 'admin_flash_error');
                    $this->redirect(array('action' => 'admin_index'));
                } else {
                    $this->Session->setFlash('Please try again! correct errors.', 'admin_flash_error');
                }
            } else {

                $this->request->data['Product']['product_image'] = $this->request->data['Product']['product_prev_image'];

                /* $this->Product->bindModel(array('hasMany' => array('ProductImage')));
                  $data = $this->Product->read(null, $id);
                  $this->request->data['ProductImage'] = $data['ProductImage']; */
            }
        } else {

            $this->Product->bindModel(array('hasMany' => array('ProductImage')));
            $this->request->data = $this->Product->read(null, $id);
        }
    }

    public function admin_product_add() {
        $this->loadModel('Category');
        $categories = $this->Category->find('list', array('conditions' => array('Category.status' => 1), 'fields' => array('id', 'category_name'), 'order' => array('Category.category_name')));
        //$categories = $this->Category->find('all',array('conditions'=>array('Category.status'=>1)));
        //pr($categories);die;
        $this->set('categories', $categories);
        if ($this->request->is('post')) {
            //pr($this->request->data);die;
            $this->Product->set($this->request->data);
           
		   if ($this->Product->validates()) {
                $file = array();

                if (isset($this->request->data['Product']['product_image']['name']) && $this->request->data['Product']['product_image']['error'] == 0) {
                    $file = $this->request->data['Product']['product_image'];

                    $thumb = array('size' => array(Configure::read('PRODUCT.THUMB_WIDTH'), Configure::read('PRODUCT.THUMB_HEIGHT')), 'type' => 'resize');
                    $large = array('size' => array(Configure::read('PRODUCT.LARGE_WIDTH'), Configure::read('PRODUCT.LARGE_HEIGHT')), 'type' => 'resize');
                    $res = $this->Upload->upload($file, Configure::read('PRODUCT.THUMB') . DS, $thumb);
                    $resl = $this->Upload->upload($file, Configure::read('PRODUCT.LARGE') . DS, $large);
                    $res1 = $this->Upload->upload($file, Configure::read('PRODUCT.MAIN_IMAGE') . DS);
                    if ($this->Upload->result) {

                        //echo $this->Upload->result;die;
                        $this->request->data['Product']['product_image'] = $this->Upload->result;
                    }
                }
                $additional_data = array();
                if (isset($this->request->data['Product']['additional_images']) && !empty($this->request->data['Product']['additional_images'])) {
                    $additional_images = array();
                    foreach ($this->request->data['Product']['additional_images'] as $key => $value) {
                        $additional_images = $this->request->data['Product']['additional_images'][$key];

                        $thumb = array('size' => array(Configure::read('PRODUCT.ADDITIONAL_THUMB_WIDTH'), Configure::read('PRODUCT.ADDITIONAL_THUMB_HEIGHT')), 'type' => 'resize');
                        $large = array('size' => array(Configure::read('PRODUCT.ADDITIONAL_LARGE_WIDTH'), Configure::read('PRODUCT.ADDITIONAL_LARGE_HEIGHT')), 'type' => 'resize');
                        $res = $this->Upload->upload($additional_images, Configure::read('PRODUCT.ADDITIONAL_THUMB') . DS, $thumb);
                        $resl = $this->Upload->upload($additional_images, Configure::read('PRODUCT.ADDITIONAL_LARGE') . DS, $large);
                        $res1 = $this->Upload->upload($additional_images, Configure::read('PRODUCT.ADDITIONAL_IMAGES') . DS);
                        if ($this->Upload->result) {

                            //echo $this->Upload->result;die;
                            $additional_data[] = $this->Upload->result;
                        }
                    }
                }
                //pr($additional_data);die;
                //pr($this->request->data);die;
                if ($this->Product->save($this->request->data)) {
                    //pr($this->request->data);die;
                    $last_id = $this->Product->id;

                    if (!empty($additional_data)) {
                        $this->loadModel('ProductImage');
                        $additional = array();
                        foreach ($additional_data as $values) {
                            $additional['ProductImage']['product_id'] = $last_id;
                            $additional['ProductImage']['images'] = $values;
                            $this->ProductImage->create();
                            $this->ProductImage->save($additional);
                        }
                    }
                    $this->Session->setFlash('Product created successfully', 'admin_flash_error');
                    $this->redirect(array('action' => 'admin_index'));
                } else {
                    $this->Session->setFlash('Please try again! correct errors.', 'admin_flash_error');
                }
            }
        }
    }

    public function admin_delete_additional_images() {
        if ($this->request->is('ajax')) {
            $this->layout = false;
            $this->autoRender = false;
            $this->loadModel('ProductImage');
            $image_id = $this->request->data['image_id'];
            //$image_name = $this->request->data['image_name'];
            $imageData = $this->ProductImage->findById($image_id);
            if (!empty($imageData)) {
                $this->ProductImage->id = $image_id;
                if ($this->ProductImage->delete()) {
                    $imageName = $imageData['ProductImage']['images'];
                    if (file_exists(Configure::read('PRODUCT.ADDITIONAL_IMAGES') . $imageName)) {
                        @unlink(Configure::read('PRODUCT.ADDITIONAL_IMAGES') . $imageName);
                    }
                    if (file_exists(Configure::read('PRODUCT.ADDITIONAL_THUMB') . $imageName)) {
                        @unlink(Configure::read('PRODUCT.ADDITIONAL_THUMB') . $imageName);
                    }
                    if (file_exists(Configure::read('PRODUCT.ADDITIONAL_LARGE') . $imageName)) {
                        @unlink(Configure::read('PRODUCT.ADDITIONAL_LARGE') . $imageName);
                    }
                }
                echo 'success';
                die;
            }
        }
    }

    public function admin_status($id, $status) {
        $status = ($status == 1) ? 0 : 1;
        $this->Product->id = $id;
        $this->Product->saveField('status', $status);
        $this->Session->setFlash('Product created successfully', 'admin_flash_error');
        $this->redirect(array('action' => 'admin_index'));
    }

    public function admin_process() {
                
		$ids=$this->request->data['Product']['id'];
		$myids=implode(",",$ids);
		$action=$this->request->data['Product']['action'];
		
		if($action=='active')
		{
			$this->Product->updateAll(
			array('Product.status' => 1),
			array('Product.id in('.$myids.')')
			);
			$this->Session->setFlash('Product Status Acitive successfully', 'admin_flash_error');
			$this->redirect(array('action' => 'admin_index'));
		}
		else if($action=='deactivate')
		{
			$this->Product->updateAll(
			array('Product.status' => 0),
			array('Product.id in('.$myids.')')
			);
			$this->Session->setFlash('Product De-Active successfully', 'admin_flash_error');
			$this->redirect(array('action' => 'admin_index'));
		}
		else
		{
			$this->Product->deleteAll(array('Product.id in('.$myids.')'), false);
			$this->Session->setFlash('Product Delete successfully', 'admin_flash_error');
			$this->redirect(array('action' => 'admin_index'));
		}
		die();
        //pr($this->request->data);die;
    }

}
