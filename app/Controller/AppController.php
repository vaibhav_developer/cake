<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
 var  $components = array('Auth','Session','Paginator');
	public function beforeFilter()
	{
		//spr($this);die;
			if(isset($this->request->params['prefix']) && $this->request->params['prefix']=='admin'){
				//echo "hi";
				$this->Auth->userModel = 'User';
				$this->Auth->authenticate = array('Form' => array('scope'=>array('role_id'=>1,'status'=>1), 'fields' => array('username' => 'email')));
				$this->Auth->loginAction = array('admin'=>true,'controller' => 'users', 'action' => 'login');
				$this->Auth->loginRedirect = array('admin'=>true,'controller' => 'users', 'action' => 'dashboard');
				//$this->Auth->logoutRedirect = array('admin'=>true,'controller' => 'users', 'action' => 'login');
				$this->Auth->autoRedirect = true;
				$this->layout = 'admin';
				$this->Auth->allow(array('admin_login'));
				//A::$sessionkey ='Auth.Admin'
				
				//pr($this);die;
				
				//echo "hi";die;
			}else{
				$this->Auth->userModel = 'User';
				$this->Auth->authenticate = array('Form' => array('scope'=>array('role_id'=>2,'status'=>1), 'fields' => array('username' => 'email')));
				$this->Auth->loginAction = array('admin'=>false,'controller' => 'users', 'action' => 'login');
				$this->Auth->loginRedirect = array('admin'=>false,'controller' => 'users', 'action' => 'dashboard');
				
			}
	
	
	}
	
}