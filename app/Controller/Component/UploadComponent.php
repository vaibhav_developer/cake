<?php 
class UploadComponent extends Component{
	public $_file;
    public $_extension;
    public $_destination;
	public $_name;
	public function upload($file=null,$destination=null,$rules=null){
		
		$this->result = false;
		$this->_file = $file;
        $this->_destination = $destination;
		if (substr($this->_destination, -1) != '/') {
            $this->_destination .= '/';
        }
		
		$fileName = $this->uniquename($destination . $file['name']);
		$fileTmp = $file['tmp_name'];
		$fileSize = $file['size'];
		$fileType = $file['type'];
		$fileError = $file['error'];
		$this->_name = $fileName;
		$this->_extension = strtolower($this->getExtension($this->_name));
		/* pr($this->_name);
		echo $this->_extension;
		pr($fileTmp);die; */
		
		if (is_uploaded_file($fileTmp)) {
			if ($rules == NULL) {
				$output = $fileName;
				// -- just upload it
				if (move_uploaded_file($fileTmp, $output)) {
					chmod($output, 0644);
					
					$this->result = basename($this->_name);
				}
			}else{
			//echo "hi";
			 // -- gd lib check
                        if (function_exists("imagecreatefromjpeg")) {
                            
                            // -- handle it based on rules
                            if (isset($rules['type']) && isset($rules['size'])) {
                                $this->resize($this->_file,$rules['size'], $this->_extension);
                            } else {
                                $this->error("Invalid \"rules\" parameter");
                            }
                        } else {
                            $this->error("GD library is not installed");
                        }
			}
		}
		
	}
	public function resize($file=null,$size=null,$type,$output = NULL){
	if (is_null($output))
            $output = 'jpg';
        
				if($type=="jpg" || $type=="jpeg" )
				{
					$src = imagecreatefromjpeg($file['tmp_name']);
				
				}
				else if($type=="png")
				{
					$src = imagecreatefrompng($file['tmp_name']);
				
				}
				else 
				{
					$src = imagecreatefromgif($file['tmp_name']);
				}
				
				list($width,$height)=getimagesize($file['tmp_name']);
				
				 if (is_array($size)) {
					$maxW = intval($size[0]);
					$maxH = intval($size[1]);
				}
				$dstImg = imagecreatetruecolor($maxW, $maxH);
                $white = imagecolorallocate($dstImg, 255, 255, 255);
                imagefill($dstImg, 0, 0, $white);
                imagecolortransparent($dstImg,$white);
				
				imagecopyresampled($dstImg,$src,0,0,0,0,$maxW,$maxH,$width,$height);
				// -- try to write
        switch ($output) {
            case 'jpg':
            case 'JPG':
                $write = imagejpeg($dstImg, $this->_name);
                break;
            case 'png':
            case 'PNG':
                $write = imagepng($dstImg, $this->_name . ".png");
                break;
            case 'gif':
            case 'GIF':
                $write = imagegif($dstImg, $this->_name . ".gif");
                break;
        }

        // -- clean up
        imagedestroy($dstImg);

        if ($write) {
            $this->result = basename($this->_name);
        } else {
            echo 'error in file uploading';
        }
				
	
	}
	function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }
public function uniquename($file) {
        $parts = pathinfo($file);
        $dir = $parts['dirname'];
        $file = ereg_replace('[^[:alnum:]_.-]', '', $parts['basename']);
        $ext = $parts['extension'];
        if ($ext) {
            $ext = '.' . $ext;
            $file = substr($file, 0, -strlen($ext));
        }
        $i = 0;
        while (file_exists($dir . '/' . $file . $i . $ext))
            $i++;
        return $dir . '/' . $file . $i . $ext;
    }
}

?>