<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 * 	
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		categories.Controller
 */
class CategoriesController extends AppController {

    var $components = array('Upload');

    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow(array('admin_add'));
    }

    public function admin_add() {
        //echo $this->Upload->add(15,6);
        if ($this->request->is('post')) {
            //pr($this->request->data);die;
            $this->Category->set($this->request->data);

            if ($this->Category->validates()) {
                $file = array();
                if (isset($this->request->data['Category']['category_image']['name']) && $this->request->data['Category']['category_image']['error'] == 0) {
                    $file = $this->request->data['Category']['category_image'];
                    $thumb = array('size' => array(Configure::read('CATEGORY.THUMB_WIDTH'), Configure::read('CATEGORY.THUMB_HEIGHT')), 'type' => 'resize');
                    //pr($thumb); die;
                    $large = array('size' => array(Configure::read('CATEGORY.LARGE_WIDTH'), Configure::read('CATEGORY.LARGE_HEIGHT')), 'type' => 'resize');
                    $res = $this->Upload->upload($file, Configure::read('CATEGORY.THUMB') . DS, $thumb);
                    $large = $this->Upload->upload($file, Configure::read('CATEGORY.LARGE') . DS, $large);
                    $res1 = $this->Upload->upload($file, Configure::read('CATEGORY.MAIN_IMAGE') . DS);
                    if ($this->Upload->result) {
                        //echo $this->Upload->result;die;
                        $this->request->data['Category']['category_image'] = $this->Upload->result;
                    }
                }
                //pr($this->request->data);die;
                if ($this->Category->save($this->request->data)) {
                    $this->Session->setFlash('Category created successfully');
                    $this->redirect(array('action' => 'cat_index'));
                } else {
                    $this->Session->setFlash('Please try again! correct errors.');
                }
            } else {
                //echo 'hi';
            }
        }
    }

    public function admin_cat_index() {
        $this->Paginator->settings = array(
            'limit' =>Configure::read('ADMIN_PAGINATION')
        );
        $categories = $this->Paginator->paginate('Category');
        $this->set('categories', $categories);
    }
	public function admin_edit_category($id=null) {
		
	
		//$category = $this->Category->find('first', array(
		//'conditions' => array('Category.id'=>$id)));
		//pr($category);die();
		//echo $id; die();
		$this->request->data =$this->Category->read(null,$id);
		//pr($this->request->data);die();
	}

}
